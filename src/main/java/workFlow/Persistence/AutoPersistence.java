package workFlow.Persistence;

import org.springframework.stereotype.Repository;
import workFlow.Models.Auto;
import workFlow.Models.Marca;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by rodri on 29/05/17.
 */
@Repository
public class AutoPersistence {
    private List<Auto> listCar;

    public AutoPersistence()
    {
        listCar = new ArrayList<Auto>();
    }

    public void addCars()
    {
        Marca m = new Marca("Ford");
        Marca f = new Marca("Fiat");

        Auto aux = new Auto(m,"ford","25","asd","123");
        Auto aux1 = new Auto(m,"focus","85000","dsa 125","321");
        Auto aux2 = new Auto(f,"Uno","25000","aaa 123","1994");

        listCar.add(aux);
        listCar.add(aux1);
        listCar.add(aux2);
    }

    public Auto getOneCar(int id)
    {
        Auto aux = null;

        for(Auto one : listCar)
        {
            if(id == one.getId())
            {
                aux = one;
            }
        }
        return aux;
    }

    public List<Auto> getAllCarsWithSameBrand(String nombre)
    {
        List<Auto> listAux = new ArrayList<Auto>();

        for(Auto aux : listCar)
        {
            if (aux.getMarca().getMarca().equals(nombre))
            {
                listAux.add(aux);
            }
        }
        return listAux;
    }

    public void saveOneCar(Auto aux)
    {
        Auto one = new Auto(aux.getMarca(),aux.getModelo(),aux.getKms(),aux.getPatente(),aux.getAnio());

        listCar.add(one);
    }

    public void saveCar(Auto one)
    {
        listCar.add(one);
    }
    public List<Auto> getListCar() {
        return listCar;
    }

    public void setListCar(List<Auto> listCar) {
        this.listCar = listCar;
    }
}
