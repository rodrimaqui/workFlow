package workFlow.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import workFlow.Models.Auto;
import workFlow.Persistence.AutoPersistence;

import java.util.List;

/**
 * Created by rodri on 31/05/17.
 */
@Service
public class AutoService {

    AutoPersistence a;

    @Autowired
    public AutoService(AutoPersistence a)
    {
        this.a = a;
    }

    public Auto getOneCar(int id)
    {
       return a.getOneCar(id);
    }

    public List<Auto> getAllCarsWithSameBrand(String nombre)
    {
        return a.getAllCarsWithSameBrand(nombre);
    }

    public void saveOneCar(Auto aux)
    {
        a.saveOneCar(aux);
    }

    public void addCars()
    {
        a.addCars();
    }

    public List<Auto> getListCar() {
        return a.getListCar();
    }


}
