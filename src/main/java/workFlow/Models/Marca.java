package workFlow.Models;

/**
 * Created by root on 24/05/17.
 */
public class Marca {

    private String marca;
    private int id;
    private static int idStatic = 0;

    public Marca(){}

    public Marca(String marca)
    {
        this.id = idStatic++;
        this.marca = marca;
    }

    public int getId() {
        return id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Marca marca1 = (Marca) o;

        if (id != marca1.id) return false;
        return marca != null ? marca.equals(marca1.marca) : marca1.marca == null;
    }

    @Override
    public int hashCode() {
        int result = marca != null ? marca.hashCode() : 0;
        result = 31 * result + id;
        return result;
    }
}
