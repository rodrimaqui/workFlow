package workFlow.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 24/05/17.
 */
public class Auto {

    private int id;
    private static int idStatic = 0;
    private Marca marca;
    private String modelo;
    private String kms;
    private String patente;
    private String anio;

    public Auto(){}

    public Auto(Marca marca,String modelo,String kms,String patente,String anio)
    {
        this.id = idStatic ++;
        this.marca = marca;
        this.modelo = modelo;
        this.kms = kms;
        this.patente = patente;
        this.anio = anio;
    }

    public int getId() {
        return id;
    }

    public Auto searchCar()
    {
        return null;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Auto auto = (Auto) o;

        if (id != auto.id) return false;
        if (marca != null ? !marca.equals(auto.marca) : auto.marca != null) return false;
        if (modelo != null ? !modelo.equals(auto.modelo) : auto.modelo != null) return false;
        if (kms != null ? !kms.equals(auto.kms) : auto.kms != null) return false;
        if (patente != null ? !patente.equals(auto.patente) : auto.patente != null) return false;
        return anio != null ? anio.equals(auto.anio) : auto.anio == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (marca != null ? marca.hashCode() : 0);
        result = 31 * result + (modelo != null ? modelo.hashCode() : 0);
        result = 31 * result + (kms != null ? kms.hashCode() : 0);
        result = 31 * result + (patente != null ? patente.hashCode() : 0);
        result = 31 * result + (anio != null ? anio.hashCode() : 0);
        return result;
    }
}
