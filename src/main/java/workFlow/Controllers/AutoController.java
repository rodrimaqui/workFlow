package workFlow.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import workFlow.Models.Auto;
import workFlow.Models.Marca;
import workFlow.Persistence.AutoPersistence;
import workFlow.Services.AutoService;

import java.util.List;

/**
 * Created by root on 24/05/17.
 */
@RestController
public class AutoController {

    @Autowired
    AutoService a;

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Auto> getAllCars()
    {
        a.addCars();

        return a.getListCar();
    }

    @RequestMapping(value = "/autos/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Auto getOneCar(@PathVariable("id") int id)
    {
        Auto one = a.getOneCar(id);
        return one;
    }

    @RequestMapping(value = "/autos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Auto> getAllMark(@RequestParam("marca") String marca)
    {
        System.out.print(a.getAllCarsWithSameBrand(marca));
        return a.getAllCarsWithSameBrand(marca);

    }

    @RequestMapping(value = "/auto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addCar(@RequestBody Auto car)
    {
        a.saveOneCar(car);
    }
}
